﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Api.RectApi;

namespace Api
{
    public static class UiApi
    {
        public static Rect GetWorldRectOfSpriteRenderer(SpriteRenderer spriteRenderer)
        {
            float xMin, xMax, yMin, yMax;
            float xSize, ySize;

            Transform transform = spriteRenderer.transform;
            Vector2 scalePixelToWorld = new Vector2(
                transform.lossyScale.x / spriteRenderer.sprite.pixelsPerUnit, transform.lossyScale.y / spriteRenderer.sprite.pixelsPerUnit);
            xSize = spriteRenderer.sprite.rect.width * scalePixelToWorld.x;
            ySize = spriteRenderer.sprite.rect.height * scalePixelToWorld.y;
            xMin = transform.position.x - spriteRenderer.sprite.pivot.x * scalePixelToWorld.x;
            yMin = transform.position.y - spriteRenderer.sprite.pivot.y * scalePixelToWorld.y;
            xMax = xMin + xSize;
            yMax = yMin + ySize;
            Rect world = new Rect(xMin, yMin, xSize, ySize);

            return world;
        }

        public static Rect GetScreenRectOfSpriteRenderer(SpriteRenderer spriteRenderer)
        {
            Rect world = GetWorldRectOfSpriteRenderer(spriteRenderer);
            return GetScreenRectOfSpriteRenderer(world);
        }

        public static Rect GetScreenRectOfSpriteRenderer(Rect world)
        {
            return world.WorldToScreen(Camera.main);
        }
    }
}

