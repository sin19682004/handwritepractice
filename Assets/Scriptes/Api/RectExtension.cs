﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Api.RectApi
{
    public static class RectExtension
    {
        public static Rect WorldToScreen(this Rect world, Camera camera)
        {
            Vector2 leftBottom = camera.WorldToScreenPoint(new Vector2(world.xMin, world.yMin));
            Vector2 topRight = camera.WorldToScreenPoint(new Vector2(world.xMax, world.yMax));
            float screenXSize = topRight.x - leftBottom.x;
            float screenYSize = topRight.y - leftBottom.y;
            Rect screen = new Rect(leftBottom.x, leftBottom.y, screenXSize, screenYSize);
            return screen;
        }
    }
}
