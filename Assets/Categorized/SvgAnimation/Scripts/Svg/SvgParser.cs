using System;
using UnityEngine;
using SvgRenderer.Shapes;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace SvgRenderer
{
    namespace Svg
    {
        public class SvgParser
        {
            public SceneNode ComputeSceneNodeFromRawSvg(string rawSvg)
            {
                List<string> animationStrings = ExtractAnimations(rawSvg);
                List<string> strokeStrings = ExtractStroke(rawSvg);
                List<string> outlineStrings = ExtractOutlines(rawSvg);

                List<Shape> shapes = new List<Shape>();
                foreach (string outlineString in outlineStrings)
                {
                    Shape shape = ConstructShape(outlineString);
                    shapes.Add(shape);
                }

                List<SegmentPathWithDuration> segmentPathWithDurations = new List<SegmentPathWithDuration>();
                for (int i = 0; i < strokeStrings.Count; i++)
                {
                    string strokeString = strokeStrings[i];
                    string animationString = animationStrings[i];
                    List<Segment> strokeSegments = ConstructStrokeSegments(strokeString);
                    List<Segment> cheatSegments = ConstructCheatSegments(strokeString);
                    var tuple = ExtractAnimationDurationAndDelay(animationString);
                    SegmentPathWithDuration segmentPathWithDuration = new SegmentPathWithDuration(duration: tuple.Item1, delay: tuple.Item2, strokeSegments, cheatSegments);
                    segmentPathWithDurations.Add(segmentPathWithDuration);
                }

                SceneNode sceneNode = new SceneNode(shapes, segmentPathWithDurations);
                return sceneNode;
            }

            protected List<string> ExtractAnimations(string rawSvg)
            {
                string pattern = @"keyframes keyframes.*?\{.*?}.*?\#.*?{.*?}";
                MatchCollection matches = Regex.Matches(rawSvg, pattern, RegexOptions.Singleline);
                //SvgDebug.Log(rawSvg);

                List<string> matchStrings = new List<string>(matches.Count);
                foreach (Match match in matches)
                {
                    //SvgDebug.Log(match.Value);
                    matchStrings.Add(match.Value);
                }
                return matchStrings;
            }

            protected List<string> ExtractStroke(string rawSvg)
            {
                string pattern = @"<path clip-path.*?></path>";
                MatchCollection matches = Regex.Matches(rawSvg, pattern, RegexOptions.Singleline);
                //SvgDebug.Log(rawSvg);

                List<string> matchStrings = new List<string>(matches.Count);
                foreach (Match match in matches)
                {
                    //SvgDebug.Log(match.Value);
                    matchStrings.Add(match.Value);
                }
                return matchStrings;
            }

            protected List<string> ExtractOutlines(string rawSvg)
            {
                string pattern = "<path d=.*fill=\".*\"></path>";
                MatchCollection matches = Regex.Matches(rawSvg, pattern);

                List<string> matchStrings = new List<string>(matches.Count);
                foreach (Match match in matches)
                {
                    //SvgDebug.Log(match.Value);
                    matchStrings.Add(match.Value);
                }
                return matchStrings;
            }

            public Shape ConstructShape(string outlineString)
            {
                string coordinateString = outlineString.Split('\"')[1];
                //SvgDebug.Log(shapeSvg);
                string[] splits = coordinateString.Split(' ');
                List<Segment> segments = ConstructSegmentList(splits);
                return new Shape(segments);
            }

            public List<Segment> ConstructStrokeSegments(string strokeString)
            {
                string pattern = ".*path clip-path=.*?d=\"(.*?)\".*";
                string replacement = "$1";
                string coordinateString = Regex.Replace(strokeString, pattern, replacement, RegexOptions.Singleline);
                string[] splits = coordinateString.Split(' ');
                List<Segment> segments = ConstructSegmentList(splits);
                return segments;
            }

            public List<Segment> ConstructCheatSegments(string strokeString)
            {
                string pattern = ".*path clip-path=.*?d=\"(.*?)\".*";
                string replacement = "$1";
                string coordinateString = Regex.Replace(strokeString, pattern, replacement, RegexOptions.Singleline);
                string[] splits = coordinateString.Split(' ');
                List<Segment> segments = ConstructSegmentList(splits);
                CheatStrokeSegment(segments[0]);
                return segments;
            }

            protected void CheatStrokeSegment(Segment segment)
            {
                Vector2 inverseVector = -segment.Vector.normalized * 64;
                segment.StartPoint = segment.StartPoint + inverseVector;
            }

            public Tuple<float, float> ExtractAnimationDurationAndDelay(string animationString)
            {
                //string pattern = @".*animation:\s\w+\s([\d\.]*?)s\s.*";
                string pattern = @".*?(\d+)%.*animation:\s\w+\s([\d\.]*?)s\s.*";
                string replacement = "$1,$2";
                string extractedString = Regex.Replace(animationString, pattern, replacement, RegexOptions.Singleline);
                string[] splits = extractedString.Split(',');

                int percentage = int.Parse(splits[0]);
                float totalTime = float.Parse(splits[1]);

                float duration = totalTime * (float)percentage / 100;
                float delay = totalTime * (float)(100 - percentage) / 100;
                return new Tuple<float, float>(duration, delay);
            }

            public List<Segment> ConstructSegmentList(string[] splits)
            {
                List<Segment> segments = new List<Segment>();
                int i = 0;
                while (ContainsMoreData(splits, i))
                {
                    Notion notion = ExtractNextNotion(splits, i);
                    CheckSvgFormatAndThrow(splits, i, notion);
                    if (ShouldMakeSegmentForCurrentNotion(notion))
                    {
                        Segment segment = MakeSegment(splits, i, notion);
                        segments.Add(segment);
                    }
                    i = ComputeNextIndexPosition(splits, i, notion);
                }
                return segments;
            }

            protected bool ContainsMoreData(string[] splits, int i)
            {
                return i < splits.Length;
            }

            protected Notion ExtractNextNotion(string[] splits, int i)
            {
                switch (splits[i])
                {
                case "M":
                    return Notion.M;
                case "Q":
                    return Notion.Q;
                case "C":
                    return Notion.C;
                case "L":
                    return Notion.L;
                case "Z":
                    return Notion.Z;
                default:
                    throw new SvgParseException("No matching notion found or index not pointing to a notion. string=" + splits[i]);
                }
            }

            protected void CheckSvgFormatAndThrow(string[] splits, int i, Notion notion)
            {
                bool isFault = false;
                switch (notion)
                {
                case Notion.Z:
                    isFault = MakePointCustom(splits, 1) != MakePointCustom(splits, i - 2);
                    break;
                case Notion.M:
                case Notion.Q:
                case Notion.C:
                case Notion.L:
                    isFault = false;
                    break;
                default:
                    throw new SvgParseException();
                }

                if (isFault)
                {
                    throw new SvgParseException();
                }
            }

            protected bool ShouldMakeSegmentForCurrentNotion(Notion notion)
            {
                switch (notion)
                {
                case Notion.M:
                case Notion.Z:
                    return false;
                case Notion.Q:
                case Notion.C:
                case Notion.L:
                    return true;
                default:
                    throw new SvgParseException();
                }
            }

            protected int ComputeNextIndexPosition(string[] fullSplitArray, int currentIndex, Notion notion)
            {
                switch (notion)
                {
                case Notion.M:
                    return currentIndex + 3;
                case Notion.Q:
                    return currentIndex + 5;
                case Notion.C:
                    return currentIndex + 7;
                case Notion.L:
                    return currentIndex + 3;
                case Notion.Z:
                    return currentIndex + 1;
                default:
                    throw new SvgParseException();
                }
            }

            protected Segment MakeSegment(string[] fullSplitArray, int currentIndex, Notion notion)
            {
                bool useLineForQ = false;
                bool useLineForC = false;

                string[] splits = fullSplitArray;
                int index = currentIndex;
                switch (notion)
                {
                case Notion.Q:
                    {
                        if (!useLineForQ)
                        {
                            Vector2 start = MakePointCustom(splits, index - 2);
                            Vector2 mid = MakePointCustom(splits, index + 1);
                            Vector2 end = MakePointCustom(splits, index + 3);

                            Vector2 p1 = 1f / 3 * start + 2f / 3 * mid;
                            Vector2 p2 = 1f / 3 * end + 2f / 3 * mid;
                            return new BezierCurve(start, p1, p2, end);
                        }
                        else
                        {
                            Vector2 start = MakePointCustom(splits, index - 2);
                            Vector2 end = MakePointCustom(splits, index + 3);
                            return new Line(start, end);
                        }
                    }
                case Notion.C:
                    {
                        if (!useLineForC)
                        {
                            Vector2 start = MakePointCustom(splits, index - 2);
                            Vector2 p1 = MakePointCustom(splits, index + 1);
                            Vector2 p2 = MakePointCustom(splits, index + 3);
                            Vector2 end = MakePointCustom(splits, index + 5);
                            return new BezierCurve(start, p1, p2, end);
                        }
                        else
                        {
                            Vector2 start = MakePointCustom(splits, index - 2);
                            Vector2 end = MakePointCustom(splits, index + 5);
                            return new Line(start, end);
                        }
                    }
                case Notion.L:
                    {
                        Vector2 start = MakePointCustom(splits, index - 2);
                        Vector2 end = MakePointCustom(splits, index + 1);
                        return new Line(start, end);
                    }
                case Notion.M:
                case Notion.Z:
                default:
                    throw new SvgParseException();
                }

            }

            protected Vector2 MakePointCustom(string[] fullSplitArray, int index)
            {
                try
                {
                    float scale = 1f;
                    int x = (int)((int.Parse(fullSplitArray[index])) * scale);
                    int y = (int)((int.Parse(fullSplitArray[index + 1]) + 124) * scale);
                    return new Vector2(x, y);
                }
                catch (FormatException e)
                {
                    throw new SvgParseException(fullSplitArray[index] + "," + fullSplitArray[index + 1]);
                }
            }

            public void TestParseNewLine()
            {
                //var r = Regex.Matches(@"sdf{fa{3
                //9}fa}hr#ga{fa}ga", @".*{[.\n\r]*}.*\#.*{.*}.*");
                string multiline = @"a
s
                b";
                var r = Regex.Matches(multiline, @"a[.\r\n]* *b");
                SvgDebug.Log(r.Count);



            }

        }

        public enum Notion
        {
            M,
            Q,
            C,
            L,
            Z,
        }

        public class SvgParseException : SvgException
        {
            public SvgParseException() : base() { }
            public SvgParseException(string message) : base(message) { }
        }

    }
}

