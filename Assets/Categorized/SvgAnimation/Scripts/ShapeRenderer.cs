using System;
using System.Collections.Generic;
using SvgRenderer.Shapes;
using System.Linq;
using UnityEngine;
using System.Text;

namespace SvgRenderer
{
    public class ShapeRenderer : IShapeRenderer
    {
        public float SamplePointDistance { get; set; } = 3f;

        protected List<List<IntersectionWithVector>> intersectionListList;
        public int Height { get; protected set; }
        public int Width { get; protected set; }

        public void Initialize(int width, int height)
        {
            Width = width;
            Height = height;
            intersectionListList?.Clear();
            intersectionListList = new List<List<IntersectionWithVector>>(Height);
            for (int i = 0; i < Height; i++)
            {
                intersectionListList.Add(new List<IntersectionWithVector>());
            }
        }

        public void AnalyzeShape(Shape shape)
        {
            Analyze(shape);
            SortIntersectionList();
        }

        public void RenderShape(IDrawer drawer, Color color)
        {
            DrawWithoutApply(drawer, color);
        }

        public void ReInitialize(int width, int height)
        {
            Initialize(width, height);
        }

        bool debugFlag = false;
        protected void Analyze(Shape shape)
        {
            List<Segment> segments = shape.Segments;
            for (int i = 0; i < segments.Count; i++)
            {
                Segment segment = segments[i];
                Segment lastSegment = (i == 0) ? segments[segments.Count - 1] : segments[i - 1];
                Segment nextSegment = (i == segments.Count - 1) ? segments[0] : segments[i + 1];
                if (segment is Line)
                {
                    AnalyzeLine((Line)segment, lastSegment, nextSegment);
                }
                else if (segment is BezierCurve)
                {
                    //debugFlag = i == 4;
                    AnalyzeBezierCurve((BezierCurve)segment);
                }
            }
        }

        protected void AnalyzeLine(Line line, Segment lastSegment, Segment nextSegment)
        {
            if (line.StartPoint.y != line.EndPoint.y)
            {
                AnalyzeLineNotHorizontal(line);
            }
            else
            {
                AnalyzeLineHorizontal(line);
            }
        }

        protected void AnalyzeLineNotHorizontal(Line line)
        {
            int yStart = Mathf.RoundToInt(line.StartPoint.y);
            int yEnd = Mathf.RoundToInt(line.EndPoint.y);
            float xStart = line.StartPoint.x;
            float xEnd = line.EndPoint.x;
            Direction direction = (yStart > yEnd) ? Direction.Down : Direction.Up;
            if (direction == Direction.Down)
            {
                for (int y = yStart; y >= yEnd; y--)
                {
                    float x = HelpMethods.Interpolation(xStart, xEnd, yStart, yEnd, y);
                    CheckXAndPrintDebugLog(x, y, xStart, xEnd, yStart, yEnd);
                    intersectionListList[y].Add(new IntersectionWithVector(x, line.Vector));
                }
            }
            else if (direction == Direction.Up)
            {
                for (int y = yStart; y <= yEnd; y++)
                {
                    float x = HelpMethods.Interpolation(xStart, xEnd, yStart, yEnd, y);
                    CheckXAndPrintDebugLog(x, y, xStart, xEnd, yStart, yEnd);
                    intersectionListList[y].Add(new IntersectionWithVector(x, line.Vector));
                }
            }
        }

        protected void AnalyzeLineHorizontal(Line line)
        {
            int yStart = Mathf.RoundToInt(line.StartPoint.y);
            int yEnd = Mathf.RoundToInt(line.EndPoint.y);
            float xStart = line.StartPoint.x;
            float xEnd = line.EndPoint.x;
            intersectionListList[yStart].Add(new IntersectionWithVector(xStart, line.Vector));
            intersectionListList[yEnd].Add(new IntersectionWithVector(xEnd, line.Vector));
        }

        protected void AnalyzeBezierCurve(BezierCurve bezierCurve)
        {
            int numberOfSamples = ComputeSampleNumber(bezierCurve);

            Vector2 lastPoint = default(Vector2);
            Vector2 lastVector = default(Vector2);
            for (int i = 0; i < numberOfSamples; i++)
            {
                float t = (float)i / (float)(numberOfSamples - 1);
                var tuple = ComputeBezierCurvePointAndSlopeByT(bezierCurve, t);
                Vector2 point = tuple.Item1;
                Vector2 vector = tuple.Item2;
                Vector2 twoPointsVector = point - lastPoint;
                if (i != 0)
                {
                    if (point.y == lastPoint.y)
                    {
                        int y = Mathf.RoundToInt(point.y);
                        if (y == point.y)
                        {
                            intersectionListList[y].Add(new IntersectionWithVector(lastPoint.x, lastVector));
                            intersectionListList[y].Add(new IntersectionWithVector(point.x, vector));
                            if (debugFlag && y == 493)
                            {
                                StringBuilder sb = new StringBuilder();
                                sb.Append(string.Format("{0}={1}{2}", nameof(lastPoint), lastPoint, Environment.NewLine));
                                sb.Append(string.Format("{0}={1}{2}", nameof(point), point, Environment.NewLine));
                                sb.Append(string.Format("{0}={1}{2}", nameof(lastVector), lastVector, Environment.NewLine));
                                sb.Append(string.Format("{0}={1}{2}", nameof(vector), vector, Environment.NewLine));
                                SvgDebug.Log(sb.ToString());
                            }
                        }
                    }
                    else
                    {
                        int yStart;
                        int yEnd;
                        int yStep;
                        Func<float, bool> func;
                        if (lastPoint.y < point.y)
                        {
                            yStart = Mathf.CeilToInt(lastPoint.y);
                            yEnd = Mathf.FloorToInt(point.y);
                            yStep = 1;
                            func = (float a) => { return a <= yEnd; };
                        }
                        else
                        {
                            yStart = Mathf.FloorToInt(lastPoint.y);
                            yEnd = Mathf.CeilToInt(point.y);
                            yStep = -1;
                            func = (float a) => { return a >= yEnd; };
                        }


                        for (int y = yStart; func(y); y += yStep)
                        {
                            float x = HelpMethods.Interpolation(lastPoint.x, point.x, lastPoint.y, point.y, y);
                            CheckXAndPrintDebugLog(x, y, lastPoint.x, point.x, lastPoint.y, point.y);
                            if (debugFlag && y == 493) //TODO: remove
                            {
                                StringBuilder sb = new StringBuilder();
                                sb.Append(string.Format("{0}={1}{2}", nameof(yStart), yStart, Environment.NewLine));
                                sb.Append(string.Format("{0}={1}{2}", nameof(yEnd), yEnd, Environment.NewLine));
                                sb.Append(string.Format("{0}={1}{2}", nameof(yStep), yStep, Environment.NewLine));
                                sb.Append(string.Format("{0}={1}{2}", nameof(lastPoint), lastPoint, Environment.NewLine));
                                sb.Append(string.Format("{0}={1}{2}", nameof(point), point, Environment.NewLine));
                                sb.Append(string.Format("{0}={1}{2}", nameof(lastVector), lastVector, Environment.NewLine));
                                sb.Append(string.Format("{0}={1}{2}", nameof(vector), vector, Environment.NewLine));
                                SvgDebug.Log(sb.ToString());
                            }
                            //Vector2 interpolatedVector = HelpMethods.Interpolation(lastVector, vector, lastPoint.y, point.y, y);
                            //intersectionListList[y].Add(new IntersectionWithVector(x, interpolatedVector));
                            intersectionListList[y].Add(new IntersectionWithVector(x, twoPointsVector));
                        }
                    }
                }
                lastPoint = point;
                lastVector = vector;
            }
        }

        protected int ComputeSampleNumber(BezierCurve bezierCurve)
        {
            float quotient = bezierCurve.Vector.magnitude / SamplePointDistance;
            return Mathf.CeilToInt(quotient) + 1;
        }

        protected Tuple<Vector2, Vector2> ComputeBezierCurvePointAndSlopeByT(BezierCurve bezierCurve, float t)
        {
            float p = 1 - t;
            float c0 = Mathf.Pow(p, 3);
            float c1 = 3 * Mathf.Pow(p, 2) * t;
            float c2 = 3 * p * Mathf.Pow(t, 2);
            float c3 = Mathf.Pow(t, 3);
            float cd1 = 3 * Mathf.Pow(p, 2);
            float cd2 = 6 * p * t;
            float cd3 = 3 * Mathf.Pow(t, 2);
            Vector2 pd1 = bezierCurve.P1 - bezierCurve.StartPoint;
            Vector2 pd2 = bezierCurve.P2 - bezierCurve.P1;
            Vector2 pd3 = bezierCurve.EndPoint - bezierCurve.P2;

            Vector2 point = c0 * bezierCurve.StartPoint + c1 * bezierCurve.P1 + c2 * bezierCurve.P2 + c3 * bezierCurve.EndPoint;
            Vector2 vector = cd1 * pd1 + cd2 * pd2 + cd3 * pd3;

            return new Tuple<Vector2, Vector2>(point, vector);
        }

        protected void SortIntersectionList()
        {
            for (int y = 0; y < Height; y++)
            {
                List<IntersectionWithVector> intersectionList = intersectionListList[y];
                intersectionList.Sort(CompareIntersection);
                //RemoveRedundentIntersections(intersectionList);
            }
        }

        protected void RemoveRedundentIntersections(List<IntersectionWithVector> intersectionList)
        {
            for (int i = 0; i < intersectionList.Count; i++)
            {
                if (i != 0 &&
                    intersectionList[i - 1].x == intersectionList[i].x)
                {
                    intersectionList.RemoveAt(i);
                    i--;
                }
            }
        }

        protected void DrawWithoutApply(IDrawer drawer, Color color)
        {
            for (int y = 0; y < Height; y++)
            {
                List<IntersectionWithVector> intersectionList = intersectionListList[y];
                int fillStripe = 0;
                for (int i = 0; i < intersectionList.Count; i++)
                {
                    IntersectionWithVector current = intersectionList[i];
                    IntersectionWithVector last = (i == 0) ? null : intersectionList[i - 1];
                    bool shouldIncreaseFillStripe = ShouldIncreaseFillStripe(intersectionList, i);
                    if (shouldIncreaseFillStripe)
                    {
                        fillStripe++;
                    }

                    if (fillStripe % 2 == 0)
                    {
                        for (int x = Mathf.RoundToInt(last.x); x < Mathf.RoundToInt(current.x); x++)
                        {
                            try
                            {
                                drawer.SetPixel(x, y, color);
                            }
                            catch (Exception e)
                            {
                                SvgDebug.Log(x + "," + y + "," + i);
                                throw new Exception("", e);
                            }
                        }
                    }
                }
                if (fillStripe % 2 == 1)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(y);
                    sb.Append(Environment.NewLine);
                    foreach (IntersectionWithVector pack in intersectionList)
                    {
                        sb.Append(string.Format("({0},{1})", pack.x, pack.vector));
                        sb.Append(Environment.NewLine);
                    }
                    throw new SvgShapeRenderException();
                    SvgDebug.Log(sb.ToString());

                    for (int t = 0; t <= 0; t++)
                    {
                        for (int x = 0; x < 30; x++)
                        {
                            drawer.SetPixel(x, y + t, Color.red);
                        }
                    }
                    for (int i = 0; i < intersectionList.Count; i++)
                    {
                        int x = Mathf.RoundToInt(intersectionList[i].x);
                        drawer.SetPixels(x - 3, y - 3, 5, 5, HelpMethods.GetColorArray(25, Color.magenta));
                    }
                }
            }
        }

        protected bool ShouldIncreaseFillStripe(List<IntersectionWithVector> intersectionList, int i)
        {
            IntersectionWithVector current = intersectionList[i];


            if (i <= 0)
            {
                return true;
            }
            if (current.vector.y == 0)
            {
                return false;
            }
            IntersectionWithVector last = intersectionList[i - 1];

            bool isSameX = current.x == last.x;
            if (!isSameX)
            {
                return true;
            }
            else
            {
                IntersectionWithVector lastNonZeroSlope = GetLastNonZeroSlopeIntersection(intersectionList, i);
                if (lastNonZeroSlope == null)
                {
                    return true;
                }
                else
                {
                    bool isCurrentPositive = current.vector.y > 0;
                    bool isLastPositive = lastNonZeroSlope.vector.y > 0;
                    if (isCurrentPositive == isLastPositive)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
        }

        protected IntersectionWithVector GetLastNonZeroSlopeIntersection(List<IntersectionWithVector> intersectionList, int currentIndex)
        {
            for (int i = currentIndex - 1; i >= 0; i--)
            {
                if (intersectionList[i].vector.y != 0)
                {
                    return intersectionList[i];
                }
            }
            return null;
        }

        public void DrawRound(IDrawer drawer, Vector2 point, int thickness, Color color)
        {
            //Rect dotBorderLoss = new Rect(point.x - thickness, point.y - thickness, 2 * thickness, 2 * thickness);
            //Rect svgBorder = new Rect(0, 0, Width - 1, Height - 1);
            //Rect dotBorder = HelpMethods.ComputeIntersectionRect(dotBorderLoss, svgBorder);
            float squareOfRadius = Mathf.Pow(thickness, 2);

            float yBottom = point.y - thickness;
            float yTop = point.y + thickness;
            int yStart = (yBottom < 0) ? 0 : Mathf.RoundToInt(yBottom);
            int yEnd = (yTop >= Height) ? Height - 1 : Mathf.RoundToInt(yTop);

            for (int y = yStart; y < yEnd; y++)
            {
                float distanceX = Mathf.Sqrt(squareOfRadius - Mathf.Pow((y - point.y), 2));
                float rLeft = point.x - distanceX;
                float rRight = point.x + distanceX;
                int xMin = (rLeft < 0) ? 0 : Mathf.RoundToInt(rLeft);
                int xMax = (rRight >= Width) ? Width - 1 : Mathf.RoundToInt(rRight);

                List<IntersectionWithVector> intersectionList = intersectionListList[y];
                int fillStripe = 0;
                for (int i = 0; i < intersectionList.Count; i++)
                {
                    IntersectionWithVector current = intersectionList[i];
                    IntersectionWithVector last = (i == 0) ? null : intersectionList[i - 1];
                    bool shouldIncreaseFillStripe = ShouldIncreaseFillStripe(intersectionList, i);
                    if (shouldIncreaseFillStripe)
                    {
                        fillStripe++;
                    }

                    if (fillStripe % 2 == 0)
                    {
                        for (int x = Mathf.RoundToInt(last.x); x < Mathf.RoundToInt(current.x); x++)
                        {
                            try
                            {
                                if (x >= xMin && x <= xMax) //TODO: Optimize required
                                {
                                    drawer.SetPixel(x, y, color);
                                }
                            }
                            catch (Exception e)
                            {
                                SvgDebug.Log(x + "," + y + "," + i);
                                throw new Exception("", e);
                            }
                        }
                    }
                }
            }

        }

        protected int CompareIntersection(IntersectionWithVector a, IntersectionWithVector b)
        {
            return a.x.CompareTo(b.x);
        }

        protected void CheckXAndPrintDebugLog(float x, int y, float xStart, float xEnd, int yStart, int yEnd)
        {
            if (x < 0 || x >= Width || float.IsNaN(x))
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(Environment.NewLine);
                sb.Append(nameof(yStart) + "=" + yStart + Environment.NewLine);
                sb.Append(nameof(yEnd) + "=" + yEnd + Environment.NewLine);
                sb.Append(nameof(xStart) + "=" + xStart + Environment.NewLine);
                sb.Append(nameof(xEnd) + "=" + xEnd + Environment.NewLine);
                sb.Append(nameof(y) + "=" + y + Environment.NewLine);
                sb.Append(nameof(x) + "=" + x + Environment.NewLine);
                SvgDebug.Log(sb.ToString());
            }
        }

        protected void CheckXAndPrintDebugLog(float x, int y, float xStart, float xEnd, float yStart, float yEnd)
        {
            if (x < 0 || x >= Width || float.IsNaN(x))
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(Environment.NewLine);
                sb.Append(nameof(yStart) + "=" + yStart + Environment.NewLine);
                sb.Append(nameof(yEnd) + "=" + yEnd + Environment.NewLine);
                sb.Append(nameof(xStart) + "=" + xStart + Environment.NewLine);
                sb.Append(nameof(xEnd) + "=" + xEnd + Environment.NewLine);
                sb.Append(nameof(y) + "=" + y + Environment.NewLine);
                sb.Append(nameof(x) + "=" + x + Environment.NewLine);
                SvgDebug.Log(sb.ToString());
            }
        }

    }

    public class IntersectionWithVector
    {
        public float x;
        public Vector2 vector;

        public IntersectionWithVector() : this(0, Vector2.zero) { }

        public IntersectionWithVector(float x, Vector2 vector)
        {
            this.x = x;
            this.vector = vector;
        }
    }

    public enum Direction
    {
        None,
        Up,
        Down,
        Left,
        Right,
    }

    public interface IShapeRenderer
    {
        void RenderShape(IDrawer drawer, Color color);
        void Initialize(int width, int height);
        void AnalyzeShape(Shape shape);
        void ReInitialize(int width, int height);
    }

    public class SvgShapeRenderException : SvgException { }

}
