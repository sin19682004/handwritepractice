using System;
using System.Collections.Generic;
using SvgRenderer.Shapes;
using System.Linq;
using UnityEngine;
using System.Text;

namespace SvgRenderer
{
    public class SceneNodeAnimator
    {
        public SceneNode SceneNode { get; protected set; }
        public IDrawer Drawer { get; protected set; }
        public List<ShapeRenderer> ShapeRenderers { get; protected set; }
        public float AccumulatedTime { get; protected set; }
        public int IndexStrokeAndDelay { get; protected set; }
        public Color DefaultColor { get; } = Color.blue;
        public List<Color> StrokeColors { get; protected set; }
        public List<Color> FinishColors { get; protected set; }

        protected List<float> strokeStartTime = new List<float>();
        protected List<float> strokeFinishTime = new List<float>();
        protected List<List<Tuple<float, bool>>> linePercentageListList = new List<List<Tuple<float, bool>>>();
        public float TotalTime { get; protected set; }
        public int Thickness { get; set; } = 68;

        public void Initialize(SceneNode sceneNode, IDrawer drawer, List<ShapeRenderer> shapeRenderers)
        {
            this.SceneNode = sceneNode;
            this.Drawer = drawer;
            this.ShapeRenderers = shapeRenderers;
            AccumulatedTime = 0;
            IndexStrokeAndDelay = 0;
            ComputeStrokeTime();
        }

        protected void ComputeStrokeTime()
        {
            float accTime = 0;
            for (int i = 0; i < SceneNode.SegmentPathWithDurations.Count; i++)
            {
                strokeStartTime.Add(accTime);
                SegmentPathWithDuration segmentPathWithDuration = SceneNode.SegmentPathWithDurations[i];
                accTime += segmentPathWithDuration.Duration;
                accTime += segmentPathWithDuration.Delay;
                strokeFinishTime.Add(accTime);

                List<Tuple<float, bool>> linePercentageList = new List<Tuple<float, bool>>();
                float sumLength = 0;
                foreach (Segment segment in segmentPathWithDuration.CheatSegments)
                {
                    if (segment is Line)
                    {
                        Line line = (Line)segment;
                        sumLength += line.Vector.magnitude;
                    }
                }
                float accLength = 0;
                linePercentageList.Add(new Tuple<float, bool>(0, false));
                foreach (Segment segment in segmentPathWithDuration.CheatSegments)
                {
                    if (segment is Line)
                    {
                        Line line = (Line)segment;
                        accLength += line.Vector.magnitude;
                        linePercentageList.Add(new Tuple<float, bool>(accLength / sumLength, false));
                    }
                }
                linePercentageListList.Add(linePercentageList);
            }
            TotalTime = accTime;
        }

        public void SetStrokeColors(List<Color> strokeColors)
        {
            if (strokeColors.Count != ShapeRenderers.Count)
            {
                throw new SvgException("strokeColors.Count=" + strokeColors.Count);
            }
            StrokeColors = strokeColors;
        }

        public void SetFinishColors(List<Color> finishColors)
        {
            if (finishColors.Count != ShapeRenderers.Count)
            {
                throw new SvgException("strokeColors.Count=" + finishColors.Count);
            }
            FinishColors = finishColors;
        }

        public void UpdateWithoutApply(float elapseTime)
        {
            AccumulatedTime += elapseTime;

            for (int i = 0; i < SceneNode.SegmentPathWithDurations.Count; i++)
            {
                if (AccumulatedTime >= strokeStartTime[i] &&
                    AccumulatedTime < strokeFinishTime[i])
                {
                    SegmentPathWithDuration segmentPathWithDuration = SceneNode.SegmentPathWithDurations[i];
                    float timeFromCurrentStroke = AccumulatedTime - strokeStartTime[i];
                    if (timeFromCurrentStroke < segmentPathWithDuration.Duration)
                    {
                        List<Tuple<float,bool>> linePercentageList = linePercentageListList[i];
                        float percentage = timeFromCurrentStroke / segmentPathWithDuration.Duration;
                        for (int j = 0; j < linePercentageList.Count - 1; j++)
                        {
                            if (percentage <= linePercentageList[j + 1].Item1)
                            {
                                Vector2 startPoint = segmentPathWithDuration.CheatSegments[j].StartPoint;
                                Vector2 endPoint = segmentPathWithDuration.CheatSegments[j].EndPoint;
                                Vector2 point = HelpMethods.Interpolation(startPoint, endPoint, linePercentageList[j].Item1, linePercentageList[j + 1].Item1, percentage);
                                Color color = StrokeColors != null ? StrokeColors[i] : DefaultColor;
                                //int x = Mathf.RoundToInt(point.x);
                                //int y = Mathf.RoundToInt(point.y);
                                //Drawer.SetPixels(x - 3, y - 3, 7, 7, HelpMethods.GetColorArray(49, color));
                                ShapeRenderers[i].DrawRound(Drawer, point, Thickness, color);
                                if (!linePercentageList[j].Item2)
                                {
                                    ShapeRenderers[i].DrawRound(Drawer, startPoint, Thickness, color);
                                    linePercentageList[j] = new Tuple<float, bool>(linePercentageList[j].Item1, true);
                                }
                                break;
                            }
                        }
                        IndexStrokeAndDelay = i * 2;
                    }
                    else
                    {
                        int index = i * 2 + 1;
                        if (IndexStrokeAndDelay != index)
                        {
                            IndexStrokeAndDelay = index;
                            Color color = FinishColors != null ? FinishColors[i] : DefaultColor;
                            ShapeRenderers[i].RenderShape(Drawer, color);
                        }
                    }
                }
            }
        }

        public bool IsAnimationOver()
        {
            return AccumulatedTime > TotalTime;
        }

    }
}