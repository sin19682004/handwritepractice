using System;
using UnityEngine;
using UnityEngine.UI;

namespace SvgRenderer
{
    [RequireComponent(typeof(RectTransform))]
    public class TextureDrawer : MonoBehaviour, IDrawer
    {
        [SerializeField] protected RawImage drawFrameImage;

        protected Texture2D simpleTexture;
        protected Color[] cacheColors;
        public int Width { get; protected set; }
        public int Height { get; protected set; }

        public void Initialize(int width, int height)
        {
            Width = width;
            Height = height;
            simpleTexture = new Texture2D(width, height);
            drawFrameImage.texture = simpleTexture;
            cacheColors = new Color[width * height];
        }

        public void DemoDraw()
        {
            for (int y = 0; y < simpleTexture.height; y++)
            {
                for (int x = 0; x < simpleTexture.width; x++)
                {
                    Color color = ((x & y) != 0 ? Color.white : Color.gray);
                    simpleTexture.SetPixel(x, y, color);
                }
            }
            simpleTexture.Apply();
        }

        protected void SetAllPixels(Color color)
        {
            for (int y = 0; y < simpleTexture.height; y++)
            {
                for (int x = 0; x < simpleTexture.width; x++)
                {
                    simpleTexture.SetPixel(x, y, color);
                }
            }
            simpleTexture.Apply();
        }

        protected float interpolation(float c, float d, float a, float b, float ab)
        {
            return c + (d - c) * (ab - a) / (b - a);
        }

        protected Color[] GetColorArray(int size, Color color)
        {
            Color[] colors = new Color[size];
            for (int i = 0; i < size; i++)
            {
                colors[i] = color;
            }
            return colors;
        }

        protected int GetIndex(int x, int y)
        {
            return x + y * Width;
        }

        protected void CheckPointAndThrow(int x, int y)
        {
            if (x < 0 || y < 0 || x >= Width || y >= Height)
            {
                throw new Exception(string.Format("x={0},y={1}", x, y));
            }
        }

        public void Apply()
        {
            simpleTexture.SetPixels(0, 0, Width, Height, cacheColors);
            simpleTexture.Apply();
        }

        public void Clear()
        {
            Color[] colors = GetColorArray(Width*Height, Color.white);
            SetPixels(0, 0, Width, Height, colors);
            Apply();
        }

        public Color ReadPixel(int x, int y)
        {
            CheckPointAndThrow(x, y);
            return cacheColors[GetIndex(x, y)];
        }

        public Color[] ReadPixels(int x, int y, int blockWidth, int blockHeight)
        {
            throw new NotImplementedException();
        }

        public void SetPixel(int x, int y, Color color)
        {
            CheckPointAndThrow(x, y);
            cacheColors[GetIndex(x, y)] = color;
        }

        public void SetPixels(int x, int y, int blockWidth, int blockHeight, Color[] colorArray)
        {
            CheckPointAndThrow(x, y);
            CheckPointAndThrow(x + blockWidth - 1, y + blockHeight - 1);
            for (int h = 0; h < blockHeight; h++)
            {
                int j = y + h;
                for (int w = 0; w < blockWidth; w++)
                {
                    int i = x + w;
                    cacheColors[GetIndex(i, j)] = colorArray[w + h * blockWidth];
                }
            }
        }

        public void Dispose()
        {
            simpleTexture = null;
            drawFrameImage.texture = null;
            cacheColors = null;
        }

    }

    public interface IDrawer : IDisposable
    {
        void Apply();
        Color ReadPixel(int x, int y);
        Color[] ReadPixels(int x, int y, int blockWidth, int blockHeight);
        void SetPixel(int x, int y, Color color);
        void SetPixels(int x, int y, int blockWidth, int blockHeight, Color[] colorArray);
        void Initialize(int width, int height);
        void Clear();
        int Width { get; }
        int Height { get; }
    }
}


