using UnityEngine;
using SvgRenderer.Shapes;
using SvgRenderer.Svg;
using TMPro;
using System.Text.RegularExpressions;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace SvgRenderer
{
    public class TestSvgAnimation : MonoBehaviour
    {
        [SerializeField] TextureDrawer textureDrawer;
        [SerializeField] TMP_InputField inputField;
        [SerializeField] GameObject gameObject;

        public SvgLoader svgLoader = new SvgLoader();
        public SvgParser svgParser = new SvgParser();
        public bool IsDrawerInitialized { get; protected set; }

        protected ShapeRenderer shapeRenderer = new ShapeRenderer();

        public void OnClickTestButton()
        {
            TestDrawAll();
        }

        public void CheckAndInitializeDrawer()
        {
            if (!IsDrawerInitialized)
            {
                float scale = 1f;
                int size = (int)(1024 * scale);
                textureDrawer.Initialize(size, size);
                IsDrawerInitialized = true;
            }
        }

        public void TestDraw()
        {
            string rawSvg = svgLoader.GetRawSvg('山');
            SceneNode sceneNode = svgParser.ComputeSceneNodeFromRawSvg(rawSvg);
            SvgDebug.Log(sceneNode.ToString());

            //TestDraw();
            Timer timer = new Timer();
            CheckAndInitializeDrawer();
            shapeRenderer.SamplePointDistance = float.Parse(inputField.text);
            timer.Start();
            foreach (Shape shape in sceneNode.Shapes)
            {
                shapeRenderer.Initialize(textureDrawer.Width, textureDrawer.Height);
                shapeRenderer.AnalyzeShape(shape);
                shapeRenderer.RenderShape(textureDrawer, Color.black);
            }
            textureDrawer.Apply();
            timer.Click("d0");
            for (int i = 0; i < 0; i++)
            {
                textureDrawer.Clear();
                timer.Click("c" + i);
                foreach (Shape shape in sceneNode.Shapes)
                {
                    shapeRenderer.Initialize(textureDrawer.Width, textureDrawer.Height);
                    shapeRenderer.AnalyzeShape(shape);
                    shapeRenderer.RenderShape(textureDrawer, Color.black);
                }
                timer.Click("d" + (i + 1));
            }
            SvgDebug.Log("\n" + timer.BuildReport());
        }

        public void TestDrawOne(int unicode)
        {
            CheckAndInitializeDrawer();
            shapeRenderer.SamplePointDistance = float.Parse(inputField.text);
            string rawSvg = svgLoader.GetRawSvg(unicode);
            SceneNode sceneNode = svgParser.ComputeSceneNodeFromRawSvg(rawSvg);
            textureDrawer.Clear();
            foreach (Shape shape in sceneNode.Shapes)
            {
                shapeRenderer.Initialize(textureDrawer.Width, textureDrawer.Height);
                shapeRenderer.AnalyzeShape(shape);
                shapeRenderer.RenderShape(textureDrawer, Color.black);
            }
            textureDrawer.Apply();
        }

        public void TestDrawOneAndAnimate(int unicode)
        {
            CheckAndInitializeDrawer();
            shapeRenderer.SamplePointDistance = float.Parse(inputField.text);
            string rawSvg = svgLoader.GetRawSvg(unicode);
            SceneNode sceneNode = svgParser.ComputeSceneNodeFromRawSvg(rawSvg);
            textureDrawer.Clear();
            List<ShapeRenderer> shapeRenderers = new List<ShapeRenderer>();
            foreach (Shape shape in sceneNode.Shapes)
            {
                ShapeRenderer shapeRenderer = new ShapeRenderer();
                shapeRenderer.Initialize(textureDrawer.Width, textureDrawer.Height);
                shapeRenderer.AnalyzeShape(shape);
                shapeRenderer.RenderShape(textureDrawer, Color.grey);
                shapeRenderers.Add(shapeRenderer);
            }
            foreach (SegmentPathWithDuration segmentPathWithDuration in sceneNode.SegmentPathWithDurations)
            {
                foreach (Segment segment in segmentPathWithDuration.Segments)
                {
                    if (segment is Line)
                    {
                        //DrawLine(textureDrawer, (Line)segment);
                    }
                }
            }
            textureDrawer.Apply();

            SceneNodeAnimator sceneNodeAnimator = new SceneNodeAnimator();
            sceneNodeAnimator.Initialize(sceneNode, textureDrawer, shapeRenderers);
            SetColors(sceneNodeAnimator);
            StartCoroutine(AnimateStrokeCoroutineMethod(sceneNodeAnimator));
        }

        public void TestDrawOneAndAnimateIndicatorDot(int unicode)
        {
            CheckAndInitializeDrawer();
            shapeRenderer.SamplePointDistance = float.Parse(inputField.text);
            string rawSvg = svgLoader.GetRawSvg(unicode);
            SceneNode sceneNode = svgParser.ComputeSceneNodeFromRawSvg(rawSvg);
            textureDrawer.Clear();
            List<ShapeRenderer> shapeRenderers = new List<ShapeRenderer>();
            foreach (Shape shape in sceneNode.Shapes)
            {
                ShapeRenderer shapeRenderer = new ShapeRenderer();
                shapeRenderer.Initialize(textureDrawer.Width, textureDrawer.Height);
                shapeRenderer.AnalyzeShape(shape);
                shapeRenderer.RenderShape(textureDrawer, Color.grey);
                shapeRenderers.Add(shapeRenderer);
            }
            textureDrawer.Apply();

            IndicatorAnimator indicatorAnimator = new IndicatorAnimator();
            indicatorAnimator.Initialize(sceneNode, textureDrawer, shapeRenderers);
            //int showCount = 0;
            //int showPeriod = 2;
            float accumulatedTime = 0;
            float capTime = 0.03f;
            indicatorAnimator.UpdateIndicatorCallback = (Vector2 point) =>
            {
                //if (++showCount == showPeriod)
                accumulatedTime += Time.deltaTime;
                if (accumulatedTime >= capTime)
                {
                    accumulatedTime = 0;
                    int x = Mathf.RoundToInt(point.x);
                    int y = Mathf.RoundToInt(point.y);
                    textureDrawer.SetPixels(x - 5, y - 5, 11, 11, HelpMethods.GetColorArray(121, Color.black));
                }
            };
            indicatorAnimator.SetInvisibleCallback = () =>
            {
                foreach (ShapeRenderer shapeRenderer in shapeRenderers)
                {
                    shapeRenderer.RenderShape(textureDrawer, Color.grey);
                }
            };
            StartCoroutine(AnimateIndicatorCoroutineMethod(indicatorAnimator));
        }

        public void TestDrawOneAndAnimateIndicatorHand(int unicode)
        {
            CheckAndInitializeDrawer();
            shapeRenderer.SamplePointDistance = float.Parse(inputField.text);
            string rawSvg = svgLoader.GetRawSvg(unicode);
            SceneNode sceneNode = svgParser.ComputeSceneNodeFromRawSvg(rawSvg);
            textureDrawer.Clear();
            List<ShapeRenderer> shapeRenderers = new List<ShapeRenderer>();
            foreach (Shape shape in sceneNode.Shapes)
            {
                ShapeRenderer shapeRenderer = new ShapeRenderer();
                shapeRenderer.Initialize(textureDrawer.Width, textureDrawer.Height);
                shapeRenderer.AnalyzeShape(shape);
                shapeRenderer.RenderShape(textureDrawer, Color.grey);
                shapeRenderers.Add(shapeRenderer);
            }
            textureDrawer.Apply();

            IndicatorAnimator indicatorAnimator = new IndicatorAnimator();
            indicatorAnimator.Initialize(sceneNode, textureDrawer, shapeRenderers);
            indicatorAnimator.UpdateIndicatorCallback = (Vector2 point) =>
            {
                int x = Mathf.RoundToInt(point.x);
                int y = Mathf.RoundToInt(point.y);
                gameObject.transform.localPosition = new Vector2(x, y);
            };
            indicatorAnimator.SetInvisibleCallback = () =>
            {
                gameObject.SetActive(false);
            };
            indicatorAnimator.SetVisibleCallback = () =>
            {
                gameObject.SetActive(true);
            };
            StartCoroutine(AnimateIndicatorCoroutineMethod(indicatorAnimator));
        }

        protected void SetColors(SceneNodeAnimator sceneNodeAnimator)
        {
            float f33 = 51f / 255;
            float f66 = 102f / 255;
            float f99 = 153f / 255;
            Color pc = new Color(0, f66, 1);
            Color pfc = new Color(0, f33, 1);
            Color sc = new Color(f33, f33, f33);
            Color sfc = Color.black;
            sceneNodeAnimator.SetStrokeColors(new List<Color> { pc, pc, sc, sc, sc });
            sceneNodeAnimator.SetFinishColors(new List<Color> { pfc, pfc, sfc, sfc, sfc });
        }

        protected IEnumerator AnimateStrokeCoroutineMethod(SceneNodeAnimator sceneNodeAnimator)
        {
            yield return null;
            while (!sceneNodeAnimator.IsAnimationOver())
            {
                yield return null;
                sceneNodeAnimator.UpdateWithoutApply(Time.deltaTime);
                textureDrawer.Apply();
            }
        }

        protected IEnumerator AnimateIndicatorCoroutineMethod(IndicatorAnimator indicatorAnimator)
        {
            yield return null;
            while (!indicatorAnimator.IsAnimationOver())
            {
                yield return null;
                indicatorAnimator.UpdateWithoutApply(Time.deltaTime);
                textureDrawer.Apply();
            }
        }

        protected void DrawLine(IDrawer drawer, Line line)
        {
            if (line.StartPoint.y <= line.EndPoint.y)
            {
                for (int y = (int)line.StartPoint.y; y <= (int)line.EndPoint.y; y++)
                {
                    float x = HelpMethods.Interpolation(line.StartPoint.x, line.EndPoint.x, line.StartPoint.y, line.EndPoint.y, y);
                    if (x >= 0 && x < drawer.Width && y >= 0 && y < drawer.Height)
                    {
                        drawer.SetPixel(Mathf.RoundToInt(x), y, Color.red);
                    }
                }
            }
            else
            {
                for (int y = (int)line.StartPoint.y; y >= (int)line.EndPoint.y; y--)
                {
                    float x = HelpMethods.Interpolation(line.StartPoint.x, line.EndPoint.x, line.StartPoint.y, line.EndPoint.y, y);
                    if (x >= 0 && x < drawer.Width && y >= 0 && y < drawer.Height)
                    {
                        drawer.SetPixel(Mathf.RoundToInt(x), y, Color.red);
                    }
                }
            }
        }

        protected IEnumerable<IEnumerable<float>> RoundDots(Vector2 point, int thickness, Rect border)
        {
            Rect dotBorderLoss = new Rect(point.x - thickness / 2, point.y - thickness / 2, thickness, thickness);
            Rect dotBorder = HelpMethods.ComputeIntersectionRect(dotBorderLoss, border);
            //for ()
            return null;
        }

        public void TestDrawAll()
        {
            CheckAndInitializeDrawer();
            shapeRenderer.SamplePointDistance = float.Parse(inputField.text);
            StartCoroutine(DrawAllCoroutineMethod());
        }
        public int drawIndex = 0;

        protected IEnumerator DrawAllCoroutineMethod()
        {
            Timer timer = new Timer();
            timer.Start();

            int errorWordCount = 0;
            int renderWordCount = 0;
            drawIndex = 0;
            foreach (string path in Directory.GetFiles(SvgLoader.SvgFolder))
            {
                if (drawIndex++ >= 1450) break;
                renderWordCount += 1;
                string rawSvg = svgLoader.GetRawSvgWithAbsolutePath(path);
                try
                {
                    DrawWithRawSvg(rawSvg);
                }
                catch (SvgShapeRenderException e)
                {
                    errorWordCount += 1;
                    SvgDebug.Log(path);
                }
                yield return null;
            }
            timer.Click("over");
            SvgDebug.Log("\n" + timer.BuildReport());
            SvgDebug.Log("Error word count=" + errorWordCount);
            SvgDebug.Log("Render word count=" + renderWordCount);
        }

        protected void DrawWithRawSvg(string rawSvg)
        {
            SceneNode sceneNode = svgParser.ComputeSceneNodeFromRawSvg(rawSvg);
            textureDrawer.Clear();
            foreach (Shape shape in sceneNode.Shapes)
            {
                shapeRenderer.Initialize(textureDrawer.Width, textureDrawer.Height);
                shapeRenderer.AnalyzeShape(shape);
                shapeRenderer.RenderShape(textureDrawer, Color.black);
            }
            textureDrawer.Apply();
        }

        public void ClearDraw()
        {
            Color[] colors = GetColorArray(textureDrawer.Width * textureDrawer.Height, Color.white);
            textureDrawer.SetPixels(0, 0, textureDrawer.Width, textureDrawer.Height, colors);
            textureDrawer.Apply();
        }

        protected Color[] GetColorArray(int size, Color color)
        {
            Color[] colors = new Color[size];
            for (int i = 0; i < size; i++)
            {
                colors[i] = color;
            }
            return colors;
        }

        public void OnClickCreateEmptyFile()
        {
            string path = Path.Combine(SvgLoader.PersistentFolder, "empty.txt");
            if (!File.Exists(path))
            {
                File.Create(path);
            }
        }

        public void OnClickTestDrawAllButton()
        {
            TestDrawAll();
        }

        public void OnClickTestDrawOneButton()
        {
            TestDrawOne('出');
            //TestDrawOne(20004);
        }

        public void OnClickTestDrawOneAndAnimationButton()
        {
            TestDrawOneAndAnimate('出');
        }

        public void OnClickTestIndicatorDotButton()
        {
            TestDrawOneAndAnimateIndicatorDot('出');
        }

        public void OnClickTestIndicatorHandButton()
        {
            TestDrawOneAndAnimateIndicatorHand('出');
        }


    }
}


