using UnityEngine;
using SvgRenderer.Shapes;
using SvgRenderer.Svg;
using System.Text;
using System;
using System.Linq.Expressions;

namespace SvgRenderer
{
    public class SvgDebug
    {
        public static void Log(object obj)
        {
            CustomDebug.Log(obj, DebugChannel.Svg);
        }

        public static void LogError(object obj)
        {
            CustomDebug.LogError(obj, DebugChannel.Svg);
        }

    }

    public class SvgException : Exception
    {
        public SvgException() : base() { }
        public SvgException(string message) : base(message) { }
        public SvgException(string message, Exception innerException) : base(message, innerException) {}
        public SvgException(Exception innerException) : base (string.Empty, innerException) {}
    }

    public static class SvgExtension
    {
    }
}



