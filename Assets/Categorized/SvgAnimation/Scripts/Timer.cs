using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace SvgRenderer
{
    public class Timer
    {
        protected List<Round> roundTickList;
        protected const string debugReportFormat = "{0,10}~{1,10}:{2,6}ms, accumulated:{3,8}ms\n";
        public const int TicksPerMillisecond = 10000;

        public long StartTick
        {
            get
            {
                return roundTickList[0].Tick;
            }
        }

        public long CurrentTick
        {
            get
            {
                return System.DateTime.UtcNow.Ticks;
            }
        }

        public void Start(string tag = "Start")
        {
            roundTickList = new List<Round>();
            roundTickList.Add(new Round(tag, CurrentTick));
        }

        public void Click(string tag = null)
        {
            if (tag == null)
            {
                tag = (roundTickList.Count).ToString();
            }
            roundTickList.Add(new Round(tag, CurrentTick));
        }

        public string BuildReport(string format = debugReportFormat)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 1; i < roundTickList.Count; i++)
            {
                string name = roundTickList[i].Name;
                string lastName = roundTickList[i - 1].Name;
                long elapse = (roundTickList[i].Tick - roundTickList[i - 1].Tick) / TicksPerMillisecond;
                long accumulated = (roundTickList[i].Tick - StartTick) / TicksPerMillisecond;
                sb.Append(string.Format(format, lastName, name, elapse, accumulated));
            }
            return sb.ToString();
        }

        public class Round
        {
            public string Name { get; protected set; }
            public long Tick { get; protected set; }

            public Round(string name, long tick)
            {
                this.Name = name;
                this.Tick = tick;
            }
        }
    }
}

