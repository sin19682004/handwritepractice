using System;
using System.Collections.Generic;
using SvgRenderer.Shapes;
using System.Linq;
using UnityEngine;
using System.Text;

namespace SvgRenderer
{
    public class IndicatorAnimator
    {
        public SceneNode SceneNode { get; protected set; }
        public IDrawer Drawer { get; protected set; }
        public List<ShapeRenderer> ShapeRenderers { get; protected set; }
        public float AccumulatedTime { get; protected set; }
        public int IndexStrokeAndDelay { get; protected set; }
        public Color DefaultColor { get; } = Color.blue;

        protected List<float> strokeStartTime = new List<float>();
        protected List<float> strokeFinishTime = new List<float>();
        protected List<List<float>> linePercentageListList = new List<List<float>>();
        public float TotalTime { get; protected set; }

        public Action<Vector2> UpdateIndicatorCallback { get; set; }
        public Action SetVisibleCallback { get; set; }
        public Action SetInvisibleCallback { get; set; }

        public void Initialize(SceneNode sceneNode, IDrawer drawer, List<ShapeRenderer> shapeRenderers)
        {
            this.SceneNode = sceneNode;
            this.Drawer = drawer;
            this.ShapeRenderers = shapeRenderers;
            AccumulatedTime = 0;
            IndexStrokeAndDelay = -1;
            ComputeStrokeTime();
        }

        protected void ComputeStrokeTime()
        {
            float accTime = 0;
            for (int i = 0; i < SceneNode.SegmentPathWithDurations.Count; i++)
            {
                strokeStartTime.Add(accTime);
                SegmentPathWithDuration segmentPathWithDuration = SceneNode.SegmentPathWithDurations[i];
                accTime += segmentPathWithDuration.Duration;
                accTime += segmentPathWithDuration.Delay;
                strokeFinishTime.Add(accTime);

                List<float> linePercentageList = new List<float>();
                float sumLength = 0;
                foreach (Segment segment in segmentPathWithDuration.Segments)
                {
                    if (segment is Line)
                    {
                        Line line = (Line)segment;
                        sumLength += line.Vector.magnitude;
                    }
                }
                float accLength = 0;
                linePercentageList.Add(0);
                foreach (Segment segment in segmentPathWithDuration.Segments)
                {
                    if (segment is Line)
                    {
                        Line line = (Line)segment;
                        accLength += line.Vector.magnitude;
                        linePercentageList.Add(accLength / sumLength);
                    }
                }
                linePercentageListList.Add(linePercentageList);
            }
            TotalTime = accTime;
        }

        public void UpdateWithoutApply(float elapseTime)
        {
            AccumulatedTime += elapseTime;

            for (int i = 0; i < SceneNode.SegmentPathWithDurations.Count; i++)
            {
                if (AccumulatedTime >= strokeStartTime[i] &&
                    AccumulatedTime < strokeFinishTime[i])
                {
                    SegmentPathWithDuration segmentPathWithDuration = SceneNode.SegmentPathWithDurations[i];
                    float timeFromCurrentStroke = AccumulatedTime - strokeStartTime[i];
                    if (timeFromCurrentStroke < segmentPathWithDuration.Duration)
                    {
                        List<float> linePercentageList = linePercentageListList[i];
                        float percentage = timeFromCurrentStroke / segmentPathWithDuration.Duration;
                        for (int j = 0; j < linePercentageList.Count - 1; j++)
                        {
                            if (percentage <= linePercentageList[j + 1])
                            {
                                Vector2 startPoint = segmentPathWithDuration.Segments[j].StartPoint;
                                Vector2 endPoint = segmentPathWithDuration.Segments[j].EndPoint;
                                Vector2 point = HelpMethods.Interpolation(startPoint, endPoint, linePercentageList[j], linePercentageList[j + 1], percentage);
                                UpdateIndicatorCallback?.Invoke(point);
                                break;
                            }
                        }
                        int index = i * 2;
                        if (IndexStrokeAndDelay != index)
                        {
                            IndexStrokeAndDelay = index;
                            SetVisibleCallback?.Invoke();
                        }
                    }
                    else
                    {
                        int index = i * 2 + 1;
                        if (IndexStrokeAndDelay != index)
                        {
                            IndexStrokeAndDelay = index;
                            SetInvisibleCallback?.Invoke();
                        }
                    }
                }
            }
        }

        public bool IsAnimationOver()
        {
            return AccumulatedTime > TotalTime;
        }

    }
}