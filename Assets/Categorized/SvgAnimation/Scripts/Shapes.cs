using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace SvgRenderer
{
    namespace Shapes
    {
        public abstract class Segment
        {
            public Vector2 StartPoint { get; set; }
            public Vector2 EndPoint { get; set; }
            public abstract Vector2 StartVector { get; }
            public abstract Vector2 EndVector { get; }

            public Vector2 Vector
            {
                get
                {
                    return HelpMethods.ComputeVector(StartPoint, EndPoint);
                }
            }

            public Segment(Vector2 startPoint, Vector2 endPoint)
            {
                this.StartPoint = startPoint;
                this.EndPoint = endPoint;
            }

            public override string ToString()
            {
                return "<Segment>" + StartPoint + "=>" + EndPoint + "</Segment>";
            }
        }

        public class Line : Segment
        {
            public override Vector2 StartVector { get { return Vector; } }
            public override Vector2 EndVector { get { return Vector; } }

            public Line(Vector2 startPoint, Vector2 endPoint) : base(startPoint, endPoint) { }
        }

        public class BezierCurve : Segment
        {
            public Vector2 P1 { get; set; }
            public Vector2 P2 { get; set; }
            protected Vector2 startVector;
            protected Vector2 endVector;
            public override Vector2 StartVector { get { return startVector; } }
            public override Vector2 EndVector { get { return endVector; } }

            public BezierCurve(Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3) : base(p0, p3)
            {
                this.P1 = p1;
                this.P2 = p2;
                startVector = HelpMethods.ComputeVector(StartPoint, P1);
                endVector = HelpMethods.ComputeVector(P2, EndPoint);
            }
        }

        public class Shape
        {
            public List<Segment> Segments { get; set; }

            public Shape(List<Segment> segments)
            {
                this.Segments = segments;
            }

            public override string ToString()
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<Shape>");
                sb.Append(Environment.NewLine);
                foreach (Segment segment in Segments)
                {
                    sb.Append(segment.ToString());
                    sb.Append(Environment.NewLine);
                }
                sb.Append("</Shape>");
                return sb.ToString();
            }
        }

        public class SceneNode
        {
            public List<Shape> Shapes { get; set; }
            public List<SegmentPathWithDuration> SegmentPathWithDurations { get; set; }

            public SceneNode(List<Shape> shapes, List<SegmentPathWithDuration> segmentPathWithDurations = null)
            {
                this.Shapes = shapes;
                this.SegmentPathWithDurations = segmentPathWithDurations;
            }

            public override string ToString()
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<SceneNode>");
                sb.Append(Environment.NewLine);
                foreach (Shape shape in Shapes)
                {
                    sb.Append(shape.ToString());
                    sb.Append(Environment.NewLine);
                }
                sb.Append("</SceneNode>");
                return sb.ToString();
            }
        }

        public class SegmentPath
        {
            public List<Segment> Segments { get; set; }

            public SegmentPath(List<Segment> segments)
            {
                this.Segments = segments;
            }
        }

        public class SegmentPathWithDuration
        {
            public float Duration { get; set; }
            public float Delay { get; set; }
            public List<Segment> Segments { get; set; }
            public List<Segment> CheatSegments { get; set; }

            public SegmentPathWithDuration(float duration, float delay, List<Segment> segments, List<Segment> cheatSegments)
            {
                this.Duration = duration;
                this.Delay = delay;
                this.Segments = segments;
                this.CheatSegments = cheatSegments;
            }
        }



        public static class HelpMethods
        {
            public static float ComputeSlope(Vector2 a, Vector2 b)
            {
                return (b.y - a.y) / (b.x - a.x);
            }

            public static Vector2 ComputeVector(Vector2 start, Vector2 end)
            {
                return end - start;
            }

            public static float Interpolation(float c, float d, float a, float b, float ab)
            {
                return c + (d - c) * (ab - a) / (b - a);
            }

            public static Vector2 Interpolation(Vector2 c, Vector2 d, float a, float b, float ab)
            {
                return c + (d - c) * (ab - a) / (b - a);
            }

            public static SlopeEnum GetSlopeEnum(float slope)
            {
                if (float.IsNaN(slope))
                {
                    return SlopeEnum.Infinite;
                }
                else if (slope == 0)
                {
                    return SlopeEnum.Zero;
                }
                else if (slope > 0)
                {
                    return SlopeEnum.Positive;
                }
                else if (slope < 0)
                {
                    return SlopeEnum.Negative;
                }
                else
                {
                    throw new SvgException();
                }
            }

            public static Rect ComputeIntersectionRect(Rect rect1, Rect rect2)
            {
                float xMin = rect1.xMin >= rect2.xMin ? rect1.xMin : rect2.xMin;
                float xMax = rect1.xMax <= rect2.xMax ? rect1.xMax : rect2.xMax;
                float yMin = rect1.yMin >= rect2.yMin ? rect1.yMin : rect2.yMin;
                float yMax = rect1.yMax <= rect2.yMax ? rect1.yMax : rect2.yMax;
                if (xMax < xMin)
                {
                    xMax = xMin;
                }
                if (yMax < yMin)
                {
                    yMax = yMin;
                }
                return new Rect(xMin, yMin, xMax - xMin, yMax - yMin);
            }

            public static Color[] GetColorArray(int size, Color color)
            {
                Color[] colors = new Color[size];
                for (int i = 0; i < size; i++)
                {
                    colors[i] = color;
                }
                return colors;
            }

        }

        public enum SlopeEnum
        {
            None,
            Positive,
            Negative,
            Zero,
            Infinite,
        }

    }
}
