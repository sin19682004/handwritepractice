using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SvgRenderer
{
    [RequireComponent(typeof(RectTransform))]
    public class TextureDrawerWithDirtyFlags : MonoBehaviour, IDrawer
    {
        [SerializeField] protected RawImage drawFrameImage;

        protected Texture2D simpleTexture;
        protected Dictionary<int, Dictionary<int, Color>> cacheColors = new Dictionary<int, Dictionary<int, Color>>();
        public int Width { get; protected set; }
        public int Height { get; protected set; }

        public void Initialize(int width, int height)
        {
            Width = width;
            Height = height;
            simpleTexture = new Texture2D(width, height);
            drawFrameImage.texture = simpleTexture;
            cacheColors = new Dictionary<int, Dictionary<int, Color>>();
        }

        public void DemoDraw()
        {
            for (int y = 0; y < simpleTexture.height; y++)
            {
                for (int x = 0; x < simpleTexture.width; x++)
                {
                    Color color = ((x & y) != 0 ? Color.white : Color.gray);
                    simpleTexture.SetPixel(x, y, color);
                }
            }
            simpleTexture.Apply();
        }

        protected void CheckPointAndThrow(int x, int y)
        {
            if (x < 0 || y < 0 || x >= Width || y >= Height)
            {
                throw new Exception(string.Format("x={0},y={1}", x, y));
            }
        }

        protected void SetAllPixels(Color color)
        {
            for (int y = 0; y < simpleTexture.height; y++)
            {
                for (int x = 0; x < simpleTexture.width; x++)
                {
                    simpleTexture.SetPixel(x, y, color);
                }
            }
            simpleTexture.Apply();
        }

        public void Apply()
        {
            foreach (var yPair in cacheColors)
            {
                foreach (var xPair in yPair.Value)
                {
                    simpleTexture.SetPixel(xPair.Key, yPair.Key, xPair.Value);
                }
            }
            simpleTexture.Apply();
            cacheColors = new Dictionary<int, Dictionary<int, Color>>();
        }

        public void Clear()
        {
            SetAllPixels(Color.white);
        }

        public Color ReadPixel(int x, int y)
        {
            throw new NotImplementedException();
        }

        public Color[] ReadPixels(int x, int y, int blockWidth, int blockHeight)
        {
            throw new NotImplementedException();
        }

        public void SetPixel(int x, int y, Color color)
        {
            CheckPointAndThrow(x, y);
            if (!cacheColors.ContainsKey(y))
            {
                cacheColors.Add(y, new Dictionary<int, Color>());
            }
            if (!cacheColors[y].ContainsKey(x))
            {
                cacheColors[y].Add(x, color);
            }
            else
            {
                cacheColors[y][x] = color;
            }
        }

        public void SetPixels(int x, int y, int blockWidth, int blockHeight, Color[] colorArray)
        {
            CheckPointAndThrow(x, y);
            CheckPointAndThrow(x + blockWidth - 1, y + blockHeight - 1);
            for (int h = 0; h < blockHeight; h++)
            {
                int j = y + h;
                for (int w = 0; w < blockWidth; w++)
                {
                    int i = x + w;
                    SetPixel(i, j, colorArray[w + h * blockWidth]);
                }
            }
        }

        public void Dispose()
        {
            simpleTexture = null;
            drawFrameImage.texture = null;
            cacheColors = null;
        }

    }


}

