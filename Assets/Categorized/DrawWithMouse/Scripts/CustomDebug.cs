


using UnityEngine;

public class CustomDebug
{
    public static void Log(object obj, DebugChannel debugChannel = DebugChannel.Default)
    {
        Debug.Log(string.Format("[{0}] {1}", debugChannel, obj));
    }

    public static void LogError(object obj, DebugChannel debugChannel)
    {
        Debug.LogError(string.Format("[{0}] {1}", debugChannel, obj));
    }
    

}

public enum DebugChannel
{
    Default,
    Svg,
}
