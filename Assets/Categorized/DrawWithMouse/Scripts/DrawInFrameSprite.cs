﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Api;

[RequireComponent(typeof(SpriteRenderer))]
public class DrawInFrameSprite : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    private Texture2D simpleTexture;
    public SpriteRenderer spriteRenderer;
    private RectTransform rectTransform;
    public LineSetter lineSetter;

    private int txSize, tySize;
    private float xMin, xMax, yMin, yMax;
    private float xSize, ySize;

    // Start is called before the first frame update
    void Start()
    {
        simpleTexture = new Texture2D(100, 100);
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = Sprite.Create(simpleTexture, new Rect(0f, 0f, 100f, 100f), new Vector2(0.5f, 0.5f), 100);
        
        txSize = simpleTexture.width;
        tySize = simpleTexture.height;

        Rect screen = UiApi.GetScreenRectOfSpriteRenderer(renderer);

        xMin = screen.xMin;
        xMax = screen.xMax;
        yMin = screen.yMin;
        yMax = screen.yMax;

        SetAllPixels(new Color(1f, 1f, 1f, 0f));

        if (false)
        {
            Debug.Log(xMin);
            Debug.Log(xMax);
            Debug.Log(yMin);
            Debug.Log(yMax);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        //Debug.Log("OnPointerDown on sprite with EventSystem interface");
        Draw(eventData.position);
    }
    public void OnPointerUp(PointerEventData eventData)
    {
        SetAllPixels(new Color(1f, 1f, 1f, 0f));
        Debug.Log(eventData.position);
        lineSetter.SetLine(eventData.position);
    }

    public void OnDrag(PointerEventData eventData)
    {
        //Debug.Log("OnDrag: " + eventData.position);
        Draw(eventData.position);
    }

    //void OnMouseDown()
    //{
    //    //Debug.Log(string.Format("{0}, {1}", "OnMouseDown", (Input.mousePosition)));
    //    //Debug.Log(string.Format("{0}, {1}", "OnMouseDown", (Camera.main.ScreenToWorldPoint(Input.mousePosition))));
    //}

    //void OnMouseOver()
    //{
    //    if (Input.GetMouseButton(0))
    //    {
    //        Debug.Log(string.Format("{0}, {1}", "OnMouseDrag", (Input.mousePosition)));
    //    }
    //}

    //void OnMouseUp()
    //{
    //    Debug.Log(string.Format("{0}, {1}", "OnMouseUp", (Input.mousePosition)));
    //    DemoDraw();
    //}

    // Update is called once per frame
    void Update()
    {
    }

    void DemoDraw()
    {
        for (int y = 0; y < simpleTexture.height; y++)
        {
            for (int x = 0; x < simpleTexture.width; x++)
            {
                Color color = ((x & y) != 0 ? Color.white : Color.gray);
                simpleTexture.SetPixel(x, y, color);
            }
        }
        simpleTexture.Apply();
    }

    void SetAllPixels(Color color)
    {
        SetAllPixels(simpleTexture, color);
    }

    void SetAllPixels(Texture2D texture2d, Color color)
    {
        for (int y = 0; y < texture2d.height; y++)
        {
            for (int x = 0; x < texture2d.width; x++)
            {
                texture2d.SetPixel(x, y, color);
            }
        }
        texture2d.Apply();
    }

    float interpolation(float c, float d, float a, float b, float ab)
    {
        return c + (d - c) * (ab - a) / (b - a);
    }

    Color[] GetColorArray(int size, Color color)
    {
        Color[] colors = new Color[size];
        for (int i = 0; i < size; i++)
        {
            colors[i] = color;
        }
        return colors;
    }

    void Draw(Vector2 mousePosition)
    {
        float xr = interpolation(0, txSize - 1, xMin, xMax, mousePosition.x);
        int x = (int)Mathf.Round(xr);
        float yr = interpolation(0, tySize - 1, yMin, yMax, mousePosition.y);
        int y = (int)Mathf.Round(yr);

        //Debug.Log(x + ", " + y);

        //simpleTexture.SetPixel(x, y, Color.blue)
        int thick = 9;
        x = (x - thick / 2);
        y = (y - thick / 2);

        if (x < 0 || x + thick >= txSize || y < 0 || y + thick >= tySize)
        {
            return;
        }

        simpleTexture.SetPixels(x, y, thick, thick, GetColorArray(thick * thick, Color.black));
        simpleTexture.Apply();
    }
}
