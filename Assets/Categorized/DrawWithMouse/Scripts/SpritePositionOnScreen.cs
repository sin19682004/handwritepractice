﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using UnityEngine.EventSystems;
using Api;

[RequireComponent(typeof(SpriteRenderer))]
public class SpritePositionOnScreen : MonoBehaviour, IPointerDownHandler
{
    Rect world;
    Rect screen;

    // Start is called before the first frame update
    void Start()
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        world = UiApi.GetWorldRectOfSpriteRenderer(spriteRenderer);

        StringBuilder sb = new StringBuilder();
        sb.Append("SpritePositionOnScreen result:\n");

        sb.Append("## World position:\n");
        sb.Append("#### xMin = " + world.xMin + "\n");
        sb.Append("#### yMin = " + world.yMin + "\n");
        sb.Append("#### xMax = " + world.xMax + "\n");
        sb.Append("#### yMax = " + world.yMax + "\n");

        screen = UiApi.GetScreenRectOfSpriteRenderer(world);

        sb.Append("## Screen position:\n");
        sb.Append("#### xMin = " + screen.xMin + "\n");
        sb.Append("#### yMin = " + screen.yMin + "\n");
        sb.Append("#### xMax = " + screen.xMax + "\n");
        sb.Append("#### yMax = " + screen.yMax + "\n");

        Debug.Log(sb);
    }
    
    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log(string.Format("{0}, {1}", "OnMouseDown", Input.mousePosition));
    }
}
