﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
public class RectTransformPostitionOnScreen : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
    Rect world;
    Rect screen;

    // Start is called before the first frame update
    void Start()
    {
        RectTransform rectTransform = GetComponent<RectTransform>();
        Rect rect = rectTransform.rect;
        float xMin, xMax, yMin, yMax;
        float xSize, ySize;

        float xLossyScale = rectTransform.lossyScale.x;
        float yLossyScale = rectTransform.lossyScale.y;
        float xPreScale = xLossyScale / rectTransform.localScale.x;
        float yPreScale = yLossyScale / rectTransform.localScale.y;

        RenderMode renderMode = GetRenderMode(GetComponent<RectTransform>());
        if (renderMode == RenderMode.WorldSpace)
        {
            xSize = rect.width * xLossyScale;
            ySize = rect.height * yLossyScale;

            float xPos = rectTransform.position.x;
            float yPos = rectTransform.position.y;
            xMin = xPos + rect.xMin * xLossyScale;
            xMax = xPos + rect.xMax * xLossyScale;
            yMin = yPos + rect.yMin * yLossyScale;
            yMax = yPos + rect.yMax * yLossyScale;

            world = new Rect(xMin, yMin, xSize, ySize);

            Vector2 leftBottom = Camera.main.WorldToScreenPoint(new Vector2(world.xMin, world.yMin));
            Vector2 topRight = Camera.main.WorldToScreenPoint(new Vector2(world.xMax, world.yMax));
            float screenXSize = topRight.x - leftBottom.x;
            float screenYSize = topRight.y - leftBottom.y;
            screen = new Rect(leftBottom.x, leftBottom.y, screenXSize, screenYSize);
        }
        else if (renderMode == RenderMode.ScreenSpaceOverlay)
        {
            xSize = rect.width * xLossyScale;
            ySize = rect.height * yLossyScale;

            float xPos = rectTransform.position.x;
            float yPos = rectTransform.position.y;
            xMin = xPos + rect.xMin * xLossyScale;
            xMax = xPos + rect.xMax * xLossyScale;
            yMin = yPos + rect.yMin * yLossyScale;
            yMax = yPos + rect.yMax * yLossyScale;

            world = new Rect(xMin, yMin, xSize, ySize);

            Vector2 leftBottom = new Vector2(world.xMin, world.yMin);
            Vector2 topRight = new Vector2(world.xMax, world.yMax);
            float screenXSize = topRight.x - leftBottom.x;
            float screenYSize = topRight.y - leftBottom.y;
            screen = new Rect(leftBottom.x, leftBottom.y, screenXSize, screenYSize);
        }

        StringBuilder sb = new StringBuilder();
        sb.Append("SpritePositionOnScreen result:\n");

        sb.Append("## World position:\n");
        sb.Append("#### xMin = " + world.xMin + "\n");
        sb.Append("#### yMin = " + world.yMin + "\n");
        sb.Append("#### xMax = " + world.xMax + "\n");
        sb.Append("#### yMax = " + world.yMax + "\n");

        sb.Append("## Screen position:\n");
        sb.Append("#### xMin = " + screen.xMin + "\n");
        sb.Append("#### yMin = " + screen.yMin + "\n");
        sb.Append("#### xMax = " + screen.xMax + "\n");
        sb.Append("#### yMax = " + screen.yMax + "\n");

        Debug.Log(sb);
    }



    RenderMode GetRenderMode(RectTransform rectTransform)
    {
        return GetRootCanvas(rectTransform).renderMode;
    }

    Canvas GetRootCanvas(RectTransform rectTransform)
    {
        Canvas directCanvas = GetDirectCanvas(rectTransform);
        return directCanvas.rootCanvas;
    }

    Canvas GetDirectCanvas(RectTransform rectTransform)
    {
        Transform p = rectTransform.parent;

        while (p != null)
        {
            Canvas r = p.GetComponent<Canvas>();
            if (r != null)
            {
                return r;
            }
            p = p.parent;
        }
        return null;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log(string.Format("{0}, {1}", "OnMouseDown", Input.mousePosition));
        RectTransform rectTransform = GetComponent<RectTransform>();
        Rect rect = rectTransform.rect;
        Vector2 v2 = new Vector2(transform.position.x, transform.position.y);
        //Debug.Log(RectTransformUtility.PixelAdjustPoint(v2, transform, GetRootCanvas(rectTransform)));
        //Debug.Log(v2);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
    }
}
