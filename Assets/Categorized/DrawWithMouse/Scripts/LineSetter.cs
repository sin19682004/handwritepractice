﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LineSetter : MonoBehaviour
{
    public RectTransform[] oRectTransforms = null;
    public DrawDirection[] drawingDirections = null;

    private int cnt = 0;

    public enum DrawDirection
    {
        Bottom,
        Right,
    }

    public void SetLineOld(Vector2 pos)
    {
        int idx = cnt++;
        if (idx > oRectTransforms.Length - 1)
        {
            return;
        }

        DrawDirection drawDirection = drawingDirections[idx];
        RectTransform rectTransform = oRectTransforms[idx];
        Rect rect = rectTransform.rect;
        Vector2 oldPos = Vector2.zero;
        if (drawDirection == DrawDirection.Bottom)
        {
            float dx = pos.x - rectTransform.position.x - (rect.xMin + rect.xMax) / 2 * rectTransform.lossyScale.x;
            float dy = pos.y - rectTransform.position.y - (rect.yMin) * rectTransform.lossyScale.y;

            oldPos = rectTransform.localPosition;
            rectTransform.localPosition = new Vector2(
                rectTransform.localPosition.x + dx / rectTransform.parent.lossyScale.x,
                rectTransform.localPosition.y + dy / rectTransform.parent.lossyScale.y
                );
            Debug.Log("Old pos=" + oldPos);
            Debug.Log("New pos=" + rectTransform.localPosition);
        }
        else if (drawDirection == DrawDirection.Right)
        {
            float dx = pos.x - rectTransform.position.x - (rect.xMax) * rectTransform.lossyScale.x;
            float dy = pos.y - rectTransform.position.y - (rect.yMin + rect.yMax) / 2 * rectTransform.lossyScale.y;

            oldPos = rectTransform.localPosition;
            rectTransform.localPosition = new Vector2(
                rectTransform.localPosition.x + dx / rectTransform.parent.lossyScale.x,
                rectTransform.localPosition.y + dy / rectTransform.parent.lossyScale.y
                );
        }
        rectTransform.DOLocalMove(oldPos, 0.6f);
        rectTransform.gameObject.SetActive(true);
    }

    public void SetLine(Vector2 cursorUpScreenPostition)
    {
        int idx = cnt++;
        if (idx > oRectTransforms.Length - 1)
        {
            return;
        }

        DrawDirection drawDirection = drawingDirections[idx];
        RectTransform rectTransform = oRectTransforms[idx];
        Rect rect = rectTransform.rect;
        Vector2 oldPos = Vector2.zero;
        if (drawDirection == DrawDirection.Bottom)
        {
            float dx = cursorUpScreenPostition.x - rectTransform.position.x - (rect.xMin + rect.xMax) / 2 * rectTransform.lossyScale.x;
            float dy = cursorUpScreenPostition.y - rectTransform.position.y - (rect.yMin) * rectTransform.lossyScale.y;

            oldPos = rectTransform.localPosition;
            rectTransform.localPosition = new Vector2(
                rectTransform.localPosition.x + dx / rectTransform.parent.lossyScale.x,
                rectTransform.localPosition.y + dy / rectTransform.parent.lossyScale.y
                );
            Debug.Log("Old pos=" + oldPos);
            Debug.Log("New pos=" + rectTransform.localPosition);
        }
        else if (drawDirection == DrawDirection.Right)
        {
            float dx = cursorUpScreenPostition.x - rectTransform.position.x - (rect.xMax) * rectTransform.lossyScale.x;
            float dy = cursorUpScreenPostition.y - rectTransform.position.y - (rect.yMin + rect.yMax) / 2 * rectTransform.lossyScale.y;

            oldPos = rectTransform.localPosition;
            rectTransform.localPosition = new Vector2(
                rectTransform.localPosition.x + dx / rectTransform.parent.lossyScale.x,
                rectTransform.localPosition.y + dy / rectTransform.parent.lossyScale.y
                );
        }
        rectTransform.DOLocalMove(oldPos, 0.6f);
        rectTransform.gameObject.SetActive(true);
    }
}
