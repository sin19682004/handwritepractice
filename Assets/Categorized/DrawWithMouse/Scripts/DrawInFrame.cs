﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(RectTransform))]
public class DrawInFrame : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerUpHandler, IPointerDownHandler
{
    private Texture2D simpleTexture;
    public RawImage drawFrameImage;
    public PointerMode pointerMode = PointerMode.In;
    private RectTransform rectTransform;
    public LineSetter lineSetter;

    private int txSize, tySize;
    private float xMin, xMax, yMin, yMax;
    private float xSize, ySize;

    public enum PointerMode {
        None,
        In,
        Out,
    }

    // Start is called before the first frame update
    void Start()
    {
        simpleTexture = new Texture2D(128, 128);
        drawFrameImage.texture = simpleTexture;

        txSize = simpleTexture.width;
        tySize = simpleTexture.height;

        rectTransform = drawFrameImage.GetComponent<RectTransform>();
        Rect rect = rectTransform.rect;

        float xPos = rectTransform.position.x;
        float yPos = rectTransform.position.y;
        xMin = xPos + rect.xMin;
        xMax = xPos + rect.xMax;
        yMin = yPos + rect.yMin;
        yMax = yPos + rect.yMax;
        xSize = rect.width;
        ySize = rect.height;

        SetAllPixels(new Color(1f, 1f, 1f, 0f));

        if (false)
        {
            Debug.Log(xMin);
            Debug.Log(xMax);
            Debug.Log(yMin);
            Debug.Log(yMax);
            Debug.Log(xSize);
            Debug.Log(ySize);
        }
    }

    string V3ToString(Vector3 v3)
    {
        return string.Format("({0} ,{1}, {2})", v3.x, v3.y, v3.z);
    }

    string V2ToString(Vector2 v2)
    {
        return string.Format("({0} ,{1})", v2.x, v2.y);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Debug.Log("PointerEnter");
        //pointerMode = PointerMode.In;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Debug.Log("PointerExit");
        //pointerMode = PointerMode.Out;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Debug.Log("PointerUp");
        SetAllPixels(new Color(1f, 1f, 1f, 0f));
        lineSetter.SetLine(eventData.position);
    }


    // Update is called once per frame
    void Update()
    {
        if (pointerMode == PointerMode.In && Input.GetMouseButton(0))
        {
            //Debug.Log(string.Format("{0}, {1}", "OnMouseDrag", V3ToString(Input.mousePosition)));
            Draw(Input.mousePosition);
        }
    }

    void DemoDraw()
    {
        for (int y = 0; y < simpleTexture.height; y++)
        {
            for (int x = 0; x < simpleTexture.width; x++)
            {
                Color color = ((x & y) != 0 ? Color.white : Color.gray);
                simpleTexture.SetPixel(x, y, color);
            }
        }
        simpleTexture.Apply();
    }

    void SetAllPixels(Color color)
    {
        for (int y = 0; y < simpleTexture.height; y++)
        {
            for (int x = 0; x < simpleTexture.width; x++)
            {
                simpleTexture.SetPixel(x, y, color);
            }
        }
        simpleTexture.Apply();
    }

    float interpolation(float c, float d, float a, float b, float ab)
    {
        return c + (d - c) * (ab - a) / (b - a);
    }

    Color[] GetColorArray(int size, Color color)
    {
        Color[] colors = new Color[size];
        for (int i = 0; i < size; i++)
        {
            colors[i] = color;
        }
        return colors;
    }

    void Draw(Vector2 mousePosition)
    {
        float xr = interpolation(0, txSize - 1, xMin, xMax, mousePosition.x);
        int x = (int)Mathf.Round(xr);
        float yr = interpolation(0, tySize - 1, yMin, yMax, mousePosition.y);
        int y = (int)Mathf.Round(yr);

        //Debug.Log(x + ", " + y);

        //simpleTexture.SetPixel(x, y, Color.blue)
        int thick = 9;
        x = (x - thick / 2);
        y = (y - thick / 2);

        if (x < 0 || x + thick >= txSize || y < 0 || y + thick >= tySize)
        {
            return;
        }

        simpleTexture.SetPixels(x, y, thick, thick, GetColorArray(thick * thick, Color.black));
        simpleTexture.Apply();
    }

}
